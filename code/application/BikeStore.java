package application;
import vehicles.Bicycle;
//Noah Gelinas 2232487

public class BikeStore {
    public static void main(String... args){
        Bicycle[] ArrOfBikes=new Bicycle[4];
        ArrOfBikes[0]  = new Bicycle("trek",2,40);
        ArrOfBikes[1]  = new Bicycle("bianchi",3,45);
        ArrOfBikes[2]  = new Bicycle("giant",1,30);
        ArrOfBikes[3]  = new Bicycle("Diamond",4,50);


        for (int i=0;i<ArrOfBikes.length;i++) {
            System.out.print("\n");
            System.out.println(ArrOfBikes[i]);         
        }

    }
}